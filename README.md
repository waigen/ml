# README #

I created this repository to keep track of and share some machine learning resources.

### What is this repository for? ###

* I started this repository as a reources for Illinois Tech's machine learning course (cs584), fall, 2017.

### How do I get set up? ###

* lrcode.R contains some functions for computing linear regression.
* gradientdescentdemo.R uses lrcode and shows the different ways you can use the functions in lrcode.R plus some other techniques built into R. 

### Contribution guidelines ###

* TODO

### Who do I talk to? ###

* Feel free to contact me
